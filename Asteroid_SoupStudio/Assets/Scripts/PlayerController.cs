using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera cameraGO;
    
    private PlayerInput _Input;

    private Vector2 _Movement;
    private Vector2 _MousePos;
    private bool _FireButton;
    private Rigidbody rb;
    private float moveSpeed = 50f;


    private void Awake()
    {
        _Input = new PlayerInput();
        rb = gameObject.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _Input.Enable();

        _Input.Player.Move.performed += OnMovement;
        _Input.Player.Move.canceled += OnMovement;

        _Input.Player.Look.performed += OnMousePos;

        _Input.Player.Fire.performed += OnFire;

    }

    private void OnDisable()
    {
        _Input.Disable();
    }

    private void OnMovement(InputAction.CallbackContext context)
    {
        _Movement = context.ReadValue<Vector2>();
    }

    private void OnMousePos(InputAction.CallbackContext context)
    {
        _MousePos = context.ReadValue<Vector2>();
    }


    void FixedUpdate()
    {
        rb.AddForce(_Movement * moveSpeed);

        Vector2 lookPosition = _MousePos - (Vector2)cameraGO.WorldToScreenPoint(rb.position);
        float angle = Mathf.Atan2(lookPosition.y, lookPosition.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, 0, angle);
        rb.MoveRotation(targetRotation);
    }

    private void OnFire(InputAction.CallbackContext context) 
    {
        _FireButton = context.ReadValueAsButton();

        if (_FireButton)
        {
            FireProjectile();
        }
    }

    private void FireProjectile()
    {
        // Calcular la dirección hacia la cual mira el jugador
        Vector2 lookDirection = _MousePos - (Vector2)cameraGO.WorldToScreenPoint(rb.position);
        lookDirection.Normalize(); // Normalizar para obtener un vector unitario

        // Obtener un proyectil del pool y configurar la dirección
        GameObject projectile = ObjectPool.instance.GetPooledObject(lookDirection);

        if (projectile != null)
        {
            // El resto de la configuración del proyectil se maneja en SetDirection
        }
    }
}

