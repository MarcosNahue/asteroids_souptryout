﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Interfaces
{
    public interface IEnemy
    {
        float SizeMultiplier { get; set; }
        float Speed { get; set; }
        int PointsToAdd { get; set; }
        int Health { get; set; }
    }
}
