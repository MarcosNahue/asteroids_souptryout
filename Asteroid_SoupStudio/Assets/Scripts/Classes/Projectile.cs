using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float speed = 15f;
    private Rigidbody rb;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Configurar la direcci�n del proyectil
    public void SetDirection(Vector2 direction)
    {
        rb.velocity = direction.normalized * speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Limit") || collision.gameObject.CompareTag("Asteroid"))
        {
            gameObject.SetActive(false);

            if (collision.gameObject.CompareTag("Asteroid"))
            {
                collision.gameObject.GetComponent<Asteroid>().TakeDamage(10);
            }
        }
    }
}
