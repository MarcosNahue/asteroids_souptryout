using Assets.Scripts.Interfaces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ovni : IEnemy
{
    public float SizeMultiplier { get; set; }
    public float Speed { get; set; }
    public int PointsToAdd { get; set; }
    public int Health { get; set; }

    public Ovni(float sizeMultiplier, float speed, int pointsToAdd, int health)
    {
        SizeMultiplier = sizeMultiplier;
        Speed = speed;
        PointsToAdd = pointsToAdd;
        Health = health;
    }
}
