using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour, Assets.Scripts.Interfaces.IEnemy
{ 
    private int health;
    private float sizeMultiplier;
    private float speed;
    private int pointsToAdd;

    public void Initialize(float sizeMultiplier, float speed, int pointsToAdd, int initialHealth)
    {
        SizeMultiplier = sizeMultiplier;
        Speed = speed;
        PointsToAdd = pointsToAdd;
        Health = initialHealth;
    }

    public int Health
    {
        get { return health; }
        set
        {
            health = 100;
        }
    }

    public float SizeMultiplier { get; set; }
    public float Speed { get; set; }

    public int PointsToAdd
    {
        get { return pointsToAdd; }
        set
        {
            if (sizeMultiplier >= 1 && sizeMultiplier <= 1.5)
            {
                pointsToAdd = 1;
            }
            else if (sizeMultiplier > 1.5 && sizeMultiplier <= 1.6)
            {
                pointsToAdd = 2;
            }
            else if (sizeMultiplier > 1.6 && sizeMultiplier <= 2)
            {
                pointsToAdd = 3;
            }
            else if (sizeMultiplier > 2 && sizeMultiplier <= 3)
            {
                pointsToAdd = 5;
            }
        }
    }

    public void TakeDamage(int damage) 
    {
        health = -damage;
    
    }

}
