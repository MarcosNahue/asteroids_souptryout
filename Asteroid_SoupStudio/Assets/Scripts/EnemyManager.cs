using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject AsteroidGO;
    private Asteroid myAsteroid;

    void Start()
    {
        StartCoroutine(SpawnAsteroids());
    }

    private IEnumerator SpawnAsteroids()
    {
        while (true)
        {
            CreateAsteroid();
            yield return new WaitForSeconds(5f);
        }
    }

    private void CreateAsteroid()
    {
        GameObject obj = Instantiate(AsteroidGO);
        myAsteroid = obj.GetComponent<Asteroid>();

        float camHeight = 2.0f * Camera.main.orthographicSize;
        float camWidth = camHeight * Camera.main.aspect;

        float x = Random.Range(-camWidth / 2.0f, camWidth / 2.0f);
        float y = Random.Range(-camHeight / 2.0f, camHeight / 2.0f);


        obj.transform.position = new Vector3(x, y, 0f);
        myAsteroid.Initialize(Random.Range(1.0f, 4.0f), Random.Range(5.0f, 30.0f), 1, 100);
    }
}
