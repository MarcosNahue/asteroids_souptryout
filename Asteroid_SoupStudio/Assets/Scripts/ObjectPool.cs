using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private Transform projectileOrigin;
    [SerializeField] private GameObject projectilePrefab;

    public static ObjectPool instance;
    private List<GameObject> lst_pooledObjects = new List<GameObject>();
    private int amountToPool = 20;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(projectilePrefab);
            obj.SetActive(false);
            lst_pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject(Vector2 direction)
    {
        for (int i = 0; i < lst_pooledObjects.Count; i++)
        {
            if (!lst_pooledObjects[i].activeInHierarchy)
            {
                // Configurar la direcci�n del proyectil antes de activarlo
                Projectile projectileScript = lst_pooledObjects[i].GetComponent<Projectile>();
                if (projectileScript != null)
                {
                    projectileScript.SetDirection(direction);
                }

                lst_pooledObjects[i].transform.position = projectileOrigin.position;
                lst_pooledObjects[i].SetActive(true);

                return lst_pooledObjects[i];
            }
        }
        return null;
    }
}
